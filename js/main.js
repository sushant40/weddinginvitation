(function($){
	
	"use strict";
	
    $(document).ready(function(){
	
		jQuery('#countdown_dashboard').countDown({
				targetDate: {
					'day': 		29, // Put the date here
					'month': 	4, // Month
					'year': 	2015, // Year
					'hour': 	0,
					'min': 		0,
					'sec': 		0,
					'omitWeeks':true
				} 
		});

		/* Hero height
		================================================== */
		var windowHeight = $(window).height();
		
		$('.hero').height( windowHeight );
		
		$(window).resize(function() {
			
			var windowHeight = $(window).height();
			$('.hero').height( windowHeight );
			
		});

		// Menu settings
		$('#menuToggle, .menu-close').on('click', function(){
			$('#menuToggle').toggleClass('active');
			$('body').toggleClass('body-push-toleft');
			$('#theMenu').toggleClass('menu-open');
		});
			
		/* Gallery
		================================================== */
		var autoAdvance = undefined;
		new Photostack( document.getElementById( 'photostack' ), {
			callback : function( item ) {
				if (autoAdvance != undefined)
				{
					clearTimeout(autoAdvance);
				}
				autoAdvance = setTimeout(function(){
					autoAdvance = undefined;
					var next = $(".photostack nav .current").next();
					if (next.length == 0)
					{
						$(".photostack nav .current").siblings()[0].click();
					}
					else
					{
						next.click();
					}
				}, 5000);
			}
		} );

			
			/* Gallery popup
		=================================================== */	
		$('.photostack').magnificPopup({
			delegate: 'a',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
					return item.el.attr('title');
				}
			},
			/* zoom: {
				enabled: true,
				duration: 300 // don't foget to change the duration also in CSS
			} */
		});
		
		//Home Background slider
		jQuery.supersized({	
		slide_interval          :   3000,		// Length between transitions
		transition              :   1, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
		transition_speed		:	700,		// Speed of transition				
		slide_links				:	'blank',	// Individual links for each slide (Options: false, 'num', 'name', 'blank')
		slides 					:  	[			// Slideshow Images
										{image : 'us/banners/banner1.jpg'},
										{image : 'us/banners/banner2.jpg'},
										{image : 'us/banners/banner3.jpg'}
									]
		});	
	
		/* Map
		================================================== */
	
		var naurnalLocation = new google.maps.LatLng(28.053193, 76.111966);
		var delhiLocation = new google.maps.LatLng(28.560528, 77.055384);

		var baratLocation = new google.maps.LatLng(28.306860, 76.583675);

		var mapOptions = {
          center: naurnalLocation,
          zoom: 11,
          overviewMapControl: false,
          rotateControl: false,
          zoomControl: false,
          streetViewControl: false,
          scaleControl: false,
          panControl: false,
          mapTypeControl: false,
          styles: [{
          	stylers: [
		      { hue: "#aa0000" },
		      { saturation: -20 }
		    ]
          }]
        };

        var map = new google.maps.Map(document.getElementById('map'),
            mapOptions);

		var naurnalMarker = new google.maps.Marker({
		  position: naurnalLocation,
		  map: map,
		  icon: 'spotlight-poi-purple.png',
		  title: 'Appar Palace'
		});

		var delhiMarker = new google.maps.Marker({
		  position: delhiLocation,
		  map: map,
		  icon: 'spotlight-poi-yellow.png',
		  title: 'Dwarka'
		});

		var bounds = new google.maps.LatLngBounds ();
		bounds.extend(naurnalLocation);
		bounds.extend(delhiLocation);
		map.fitBounds(bounds);

		var horseMarker = new google.maps.Marker({
		  position: baratLocation,
		  map: map,
		  icon: 'mapHorseL.png',
		  title: 'Barat'
		});


		var lastTop = 0;
		$(document).scroll(function(){

			var windowHeight  = $(window).height(),
				mapHeight     = $("#map").height(),
				scrollTop     = $(window).scrollTop(),
				windowBottom  = (windowHeight + scrollTop),
			    mapOffset     = $("#map").offset().top,
			    mapTop        = (mapOffset - scrollTop),
			    mapBottom     = (scrollTop + windowHeight - mapOffset - mapHeight),
			    steps         = mapBottom + mapTop;

			//console.log('mapTop:'+mapTop);
			//console.log('mapBottom:'+mapBottom);
			//console.log('steps'+steps);

			if (lastTop > scrollTop)
			{
				horseMarker.setIcon('mapHorseL.png');
			}
			else
			{
				horseMarker.setIcon('mapHorseR.png');
			}
			lastTop = scrollTop;

			if (mapTop > 0)
			{
				//scrolled past
				if (mapBottom > 0)
				{
					var lat1 = naurnalLocation.lat();
					var lng1 = naurnalLocation.lng();
					var lat2 = delhiLocation.lat();
					var lng2 = delhiLocation.lng();

					//inview
					var percent = (windowHeight - mapHeight - mapTop)/(windowHeight - mapHeight);
					horseMarker.setPosition(new google.maps.LatLng(
						((lat1 - lat2)*percent)+lat2,
						((lng1 - lng2)*percent)+lng2));
				}
				else
				{
					//anchor bottom
					horseMarker.setPosition(delhiLocation);
				}
			}
			else
			{
				//anchor top
				horseMarker.setPosition(naurnalLocation);
			}

		}).scroll();
	});		


})(jQuery);